<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://ngaa.men
 * @since             1.0.0
 * @package           Ngaacore
 *
 * @wordpress-plugin
 * Plugin Name:       NGaa Core
 * Plugin URI:        http://ngaa.men
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            NGaa.men
 * Author URI:        http://ngaa.men
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ngaacore
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'NGAA_CORE_PATH', plugin_dir_path( __FILE__ ) );
define( 'NGAA_CORE_URL', plugin_dir_url( __FILE__ ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ngaacore-activator.php
 */
function activate_ngaacore() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ngaacore-activator.php';
	Ngaacore_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ngaacore-deactivator.php
 */
function deactivate_ngaacore() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ngaacore-deactivator.php';
	Ngaacore_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ngaacore' );
register_deactivation_hook( __FILE__, 'deactivate_ngaacore' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ngaacore.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ngaacore() {

	$plugin = new Ngaacore();
	$plugin->run();

}
run_ngaacore();
