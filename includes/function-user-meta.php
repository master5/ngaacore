<?php 

/**
 * Add user meta field
 *
 * @return void
 **/
add_filter( 'user_contactmethods', 'ngaa_add_user_meta_field' );
function ngaa_add_user_meta_field( $profile_fields ) {
	// Add new fields
	$profile_fields['kabheen_facebook'] 	= esc_html__( 'Facebook URL', 'tokoo' );
	$profile_fields['kabheen_twitter'] 		= esc_html__( 'Twitter URL', 'tokoo' );
	$profile_fields['kabheen_pinterest'] 	= esc_html__( 'Pinterest URL', 'tokoo' );
	$profile_fields['kabheen_instagram'] 	= esc_html__( 'Instagram URL', 'tokoo' );

	return $profile_fields;
}