<?php

/**
 * Fired during plugin activation
 *
 * @link       http://ngaa.men
 * @since      1.0.0
 *
 * @package    Ngaacore
 * @subpackage Ngaacore/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ngaacore
 * @subpackage Ngaacore/includes
 * @author     NGaa.men <hello@ngaa.men>
 */
class Ngaacore_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
