<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://ngaa.men
 * @since      1.0.0
 *
 * @package    Ngaacore
 * @subpackage Ngaacore/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ngaacore
 * @subpackage Ngaacore/includes
 * @author     NGaa.men <hello@ngaa.men>
 */
class Ngaacore_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
